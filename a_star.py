#! /bin/bash python
#
############################################################################################################
# Author: Michael Kalab
# michi.kalab@gmail.com
# code for calculation of the fastest way from one city to other city in Romania
# works for other instances as well, however the heuristic distance needs to be CONSISTENT
############################################################################################################



l = ['arad','zerind','oradea','timisoara','lugoj','mehadia','dobreta','craiova','sibiu','fagaras','rimnicu vilcea','pitesti','bucharest']

connects = [[1,3,8],[0,2],[1,8],[0,4],[3,5],[4,6],[5,7],[6,10,11],[0,2,9,10],[8,12],[7,8,11],[7,10,12],[9,11]]
weights = [[75,118,140],[75,71],[71,151],[118,111],[111,70],[70,75],[75,120],[120,146,138],[140,151,99,80],[99,211],[146,80,97],[138,97,101],[211,101]]

INF = float('inf')
# g (currently optimal costs) and f (currently best heuristics) ar initially infinite
g = [INF,INF,INF,INF,INF,INF,INF,INF,INF,INF,INF,INF,INF]
f = [INF,INF,INF,INF,INF,INF,INF,INF,INF,INF,INF,INF,INF]

# initialize g(arad) as 0, could do that for all cities
g[0]=0 

# vector of predecessors
preds = [0,0,0,0,0,0,0,0,0,0,0,0,0]

h = [366,374,380,329,0,0,0,160,253,176,193,100,0]
f[0]=h[0]

# list of currently opened nodes
opened = [0];

# list of currently closed nodes
closed = [];
current = 0;

# len(opened) true if and only if it is not empty
while(len(opened)):
    minimum = max
    
    # look for most promising next node
    for node in opened:
        if f[node] < minimum:
            minimum = f[node]
            current = node

    opened.remove(current)
    closed.append(current)
    
    neighbor_index = 0
    for neighbor in connects[current]:
        if neighbor in closed:
            neighbor_index = neighbor_index + 1
            continue
        
        tentative = g[current] +  weights[current][neighbor_index]
        
        if neighbor not in opened:
            opened.append(neighbor)
            
        elif tentative >= g[neighbor]:
            neighbor_index = neighbor_index + 1
            continue
        
        neighbor_index = neighbor_index + 1
        
        preds[neighbor] = current
        g[neighbor] = tentative
        f[neighbor] = g[neighbor] + h[neighbor]
        
shortest = []

current = 12
while(current != 0):
    shortest.append(current)
    current = preds[current]

print('arad')
while(len(shortest)):
    print(l[shortest[-1]])
    shortest.pop()

    
    
