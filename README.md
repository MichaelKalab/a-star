# A-star

A-star algorithm for finding the shortest path in a metric space applied
to the network of greater cities in Romania. For further information have a look
on https://en.wikipedia.org/wiki/A*_search_algorithm.